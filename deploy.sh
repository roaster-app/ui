#!/usr/bin/env bash

rm -r ./build
rm -rf ./node_modules
npm install --production
npm run build
docker-compose down --rmi local
docker-compose up