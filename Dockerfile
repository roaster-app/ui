FROM node:12.3.1-alpine

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY ./build ./build
COPY ./server.js ./

EXPOSE 3000

CMD ["npm", "run", "serve"]