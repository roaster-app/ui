export const initialAuthState = {
  isAuthenticated: false,
  token: {},
  user: {},
  error: {},
  apiDown: true
}

export const auth = (state = initialAuthState, action) => {
  switch (action.type) {
    case 'SET_ERROR':
      return {
        ...state,
        error: action.payload
      }
      case 'HEALTH_CHECK':
        return {
          ...state,
          apiDown: false
        }
      case 'HEALTH_CHECK_ERROR':
        return {
          ...state,
          apiDown: true
        }
    case 'SET_CURRENT_USER':
      return {
        ...state,
        isAuthenticated: action.payload !== 'undefined',
        token: action.payload
      }
    case 'UNSET_CURRENT_USER':
      return initialAuthState
    case 'GET_CURRENT_USER':
      return {
        ...state,
        user: action.payload
      }
    default: return state
  }
}