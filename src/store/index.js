import thunk from 'redux-thunk'
import { applyMiddleware, compose, createStore } from 'redux'
import { routerMiddleware } from 'connected-react-router'

import history from '../routes/history'
import createRootReducer from './reducers'
import { initialAuthState } from './reducers/auth'


const configureStore = preloadedState => createStore(
  createRootReducer(history),
  preloadedState,
  compose(
    applyMiddleware(
      routerMiddleware(history),
      thunk,
    )
  )
)

const store = configureStore({
  auth: initialAuthState
})

export default store