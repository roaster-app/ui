import axios from 'axios'
import jwt from 'jsonwebtoken'
import { setAuthToken } from '../../utils/jwt'
import { setError } from './errors'

export const setCurrentUser = token => {
  setAuthToken(token)
  localStorage.setItem('token', token )
  const decoded = jwt.decode(token)
  
  return {
    type: 'SET_CURRENT_USER',
    payload: decoded
  }
}

export const auth = data => {
  return async dispatch => {
    try {
      const res = await axios.post( 'http://localhost:9000/api/users/authenticate', data)
      const token = res.data.token

      dispatch(setCurrentUser(token))
    } catch (error) {
      dispatch(setError(error))
    }
  }
}

export const logout = () => {
  return async dispatch => {
    try {
      setAuthToken()
      localStorage.removeItem('token')

      dispatch({
        type: 'UNSET_CURRENT_USER',
      })

    } catch (error) {
      console.log(error);
    }
  }
}
