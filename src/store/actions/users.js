import axios from 'axios'
import { setError } from './errors'
import { auth } from './auth'

export const create = user => {
  return async dispatch => {
    try {
      const res = await axios.post('http://localhost:9000/api/users/register', user)
      
      if (res && res.status === 200) {

        dispatch(auth({
          username: user.username,
          password: user.password,
        }))
      }
    } catch (error) {
      dispatch(setError(error))
    }
  }
}

export const getCurrent = () => {
  return async dispatch => {
    try {
      const res = await axios.get('http://localhost:9000/api/users/current')
      const data  = res.data

      dispatch({
        type: 'GET_CURRENT_USER',
        payload: data
      })
    } catch (error) { }
  }
}