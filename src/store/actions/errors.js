import axios from 'axios'

export const setError = err => {
  return {
    type: 'SET_ERROR',
    payload: err
  }
}

export const healthCheck = () => {
  return async dispatch => {
    try{
      const res = await axios( 'http://localhost:9000/api/health-check' )

      if ( res && res.statusText === 'OK') {
        dispatch({
          type: 'HEALTH_CHECK',
        })
      }else{
        dispatch({
          type: 'HEALTH_CHECK_ERROR',
        })
      }
      
    } catch(e) { }
  }
  
}

