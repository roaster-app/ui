import React from 'react'
import WifiOff from '@material-ui/icons/WifiOffOutlined'
import { Typography, Link, Grid, Container } from '@material-ui/core'

const NotConnectedComponent = () => {

  const styleNotRes = {
    height: '85vh',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  }

  const refreshPage = () => { 
    window.location.reload(); 
  }

  return (
    <Container style={styleNotRes}>
      <Typography component="h1" variant="h5">
        Api is not responding or it's down
      </Typography>
      <WifiOff style={{ margin: '10px' }} />
      <Grid>
        <Link onClick={refreshPage} href="" variant="body2">
          Refresh
        </Link> the page or try again later
      </Grid>
    </Container>
  )
}

export default NotConnectedComponent