import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { CircularProgress } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  progress: {
    margin: theme.spacing(2),
  },
  small: {
    color: '#6798e5',
    animationDuration: '550ms',
  }
}))

const LoaderComponent = props => {
  const classes = useStyles()

  if(props.small){
    return <CircularProgress
      variant="indeterminate"
      disableShrink
      className={classes.small}
      size={24}
      thickness={4}
    />
  }
  

  return <CircularProgress className={classes.progress} />

}

export default LoaderComponent