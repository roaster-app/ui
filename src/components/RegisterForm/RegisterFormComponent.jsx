import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { create } from '../../store/actions/users'
import { Link as Rlink } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import {
  Container,
  Typography,
  TextField,
  Button,
  CssBaseline,
  Grid,
  Link
  //SnackbarContent,
} from '@material-ui/core'

const RegisterFormComponent = ({props}) => {
  const dispatch = useDispatch();
  const [ values, setValues ] = useState({
    username: '',
    password: '',
    email: '',
    "name.first": '',
    "name.last": '',
  })

  const handleChange = e => {
    setValues({
      ...values,
      [e.target.name]: e.target.value
    })
  }

  const submitRegister = async e => {
    e.preventDefault()

    await dispatch(create(values))

    props.history.push('/user/profile')
  }

  const classes = useStyles()

  return(
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">Sign Up</Typography>
        
        <form onSubmit={submitRegister}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            label="Username"
            name="username"
            autoFocus
            onChange={e =>handleChange(e)}
          />

          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            onChange={e =>handleChange(e)}
          />

          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="email"
            label="Email"
            type="email"
            id="email"
            onChange={e =>handleChange(e)}
          />
          <div className={classes.divided}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              name="name.first"
              label="First Name"
              id="name.first"
              onChange={e =>handleChange(e)}
            />

            <TextField
              className={classes.small}
              variant="outlined"
              margin="normal"
              required
              name="name.last"
              label="Last Name"
              id="name.last"
              onChange={e =>handleChange(e)}
            />
          </div>

          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            size="large"
            className={classes.submit}
          >
            Sign Up
          </Button>

          <Grid container>
            <Grid item>
              <Link component={Rlink} to="/login" variant="body2">
                {"Already have an account? Sign In"}
              </Link>
            </Grid>
          </Grid>

        </form>
      </div>
    </Container>
  )

}

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white,
    },
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  divided: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    
  },
  small: {
    marginLeft: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },

  // SnackBar error
  error: {
    backgroundColor: theme.palette.error.dark,
    margin: theme.spacing(1),
    width: '100%',
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
  icon: {
    fontSize: 20,
    opacity: 0.9,
    marginRight: theme.spacing(1),
  },
}))

export default RegisterFormComponent
