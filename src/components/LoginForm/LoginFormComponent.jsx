import React, { useState } from 'react'
import { auth } from '../../store/actions/auth'
import { useDispatch, useSelector } from 'react-redux'
import { Link as Rlink } from 'react-router-dom'

import { makeStyles } from '@material-ui/core/styles'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import ErrorIcon from '@material-ui/icons/Error'
import {
  Container,
  Typography,
  Avatar,
  TextField,
  FormControlLabel,
  Checkbox,
  Button,
  Grid,
  Link,
  CssBaseline,
  SnackbarContent,
} from '@material-ui/core'


const LoginFormComponent = ({props}) => {
  const dispatch = useDispatch()
  const [ values, setValues ] = useState({
    username: '',
    password: ''
  })
  const { error } = useSelector(state => state.auth)

  let errMsg = error.message === 'Request failed with status code 400'

  const handleChange = e => {
    setValues({
      ...values,
      [e.target.name]: e.target.value
    })
  }

  const submitLogin = async e => {
    e.preventDefault()

    if(errMsg){
      errMsg = undefined
    }

    await dispatch(auth(values))

    if(!errMsg){
      props.history.push('/user/profile')
    }
  }

  const classes = useStyles()
  let renderError = null

  if (values.username || values.password) {
    renderError = null
  }

  if (errMsg && !values.username && !values.password) {
    renderError = <SnackbarContent
      className={classes.error}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <ErrorIcon className={classes.icon} />
          Wrong username or password, please try again.
        </span>
      }
    />
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>

        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>

        <Typography component="h1" variant="h5">Sign in</Typography>
        {renderError}
        <form onSubmit={submitLogin}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            label="Username"
            name="username"
            autoComplete="username"
            autoFocus
            onChange={e =>handleChange(e)}
          />

          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="password"
            onChange={e =>handleChange(e)}
          />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />

          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            size="large"
            className={classes.submit}
          >
            Sign In
          </Button>

          <Grid container>
            <Grid item xs>
              <Link href="#" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link component={Rlink} to="/register" variant="body2">
                {"Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  )
}

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white,
    },
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },

  // SnackBar error
  error: {
    backgroundColor: theme.palette.error.dark,
    margin: theme.spacing(1),
    width: '100%',
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
  icon: {
    fontSize: 20,
    opacity: 0.9,
    marginRight: theme.spacing(1),
  },
}))

export default LoginFormComponent