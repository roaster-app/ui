import React from 'react'
import { useSelector } from 'react-redux'
import { Switch, Redirect, Route } from 'react-router-dom'

import Home from '../pages/Home/Home'
import Login from '../pages/Login/Login'
import Register from '../pages/Register/Register'
import HeaderComponent from '../components/Layout/Header/HeaderComponent'

const UnLoggedRoutes = props => {
  const { isAuthenticated } = useSelector(state => state.auth)
  const token = localStorage.token

  const Routes = <>
    <HeaderComponent {...{ props }} />
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/register" component={Register} />
      <Redirect to='/404' />
    </Switch>
  </>

  return (typeof token === 'undefined' && !isAuthenticated) ? Routes : <Redirect to='/user/profile' />
}

export default UnLoggedRoutes