import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Switch, Redirect, Route } from 'react-router-dom'

import { setCurrentUser } from '../store/actions/auth'

import Profile from '../pages/Profile/Profile'
import LoaderComponent from '../components/Loader/LoaderComponent'

import ContentHOC from '../components/Layout/Content/ContentComponent'

const LoggedRoutes = ({ props }) => {
  const dispatch = useDispatch()
  const { isAuthenticated } = useSelector(state => state.auth)
  const token = localStorage.token
  
  useEffect(() => {
    if (token && typeof token !== 'undefined' && ! isAuthenticated) {
      dispatch(setCurrentUser(localStorage.token))
    }
  }, [dispatch, isAuthenticated, token])

  if (!token && typeof token === 'undefined' && !isAuthenticated) {
    return <Redirect to='/login' />
  }

  if (token && typeof token !== 'undefined' && isAuthenticated) {
    return (
      <Switch>
        <Redirect exact path="/user" to="/user/profile" />
        <Route exact path="/user/profile" component={Profile} {...{ props }} />
        <Redirect to='/404' />
      </Switch>
    )
  } else {
    return <LoaderComponent />
  }
}

export default React.memo(ContentHOC(LoggedRoutes))