import React, { useEffect, useState } from 'react'
import { ConnectedRouter } from 'connected-react-router'
import { Switch, Route, Redirect } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import history from './history'

import LoggedRoutes from './LoggedRoutes'
import UnLoggedRoutes from './UnLoggedRoutes'
import NotFound from '../pages/NotFound/NotFound'

import LoaderComponent from '../components/Loader/LoaderComponent'
import NotConnectedComponent from '../components/NotConnected/NotConnectedComponent'
import { healthCheck } from '../store/actions/errors'

import { Container } from '@material-ui/core'

const Routes = () => {
  const dispatch = useDispatch()
  const { apiDown } = useSelector(state => state.auth)
  const [displayErr, setDisplayErr] = useState(false)
  const [timer, setTimer] = useState(null)
  const [recheck, setRecheck] = useState(null)

  useEffect(() => {

    dispatch(healthCheck())

    if(!apiDown){
      if(timer) clearTimeout(timer)
      if(recheck) clearInterval(recheck)
      return
    }

    let tries = 0

    setTimer(setTimeout(() =>
      setRecheck(setInterval(() => {
        tries++
        if(tries <= 3){
          dispatch(healthCheck())
        }else{
          clearInterval(recheck);
          setDisplayErr(true)
        }
      }, 5000))
    , 5000 ))
    
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, apiDown])

  const loaderContent = {
    height: '85vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  }

  if(apiDown){
    return displayErr ? <NotConnectedComponent /> : <Container component="main" style={loaderContent}>
      <LoaderComponent />
    </Container>
  }else{
    return (
      <ConnectedRouter history={history}>
        <Switch>
          <Route path="/404" component={NotFound} />
          <LoggedRoutes path="/user" component={LoggedRoutes} />
          <UnLoggedRoutes path="/" component={UnLoggedRoutes} />
          <Redirect to="/404" />
        </Switch>
      </ConnectedRouter>
    )
  }
  
}

export default Routes