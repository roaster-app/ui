import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { setCurrentUser } from '../../store/actions/auth'
import { getCurrent } from '../../store/actions/users'

import { objectIsEmpty } from '../../utils/custom-validator'
import { setAuthToken } from '../../utils/jwt'

import LoaderComponent from '../../components/Loader/LoaderComponent'

const AuthLoader = (WrappedComponent, props) => {
  const dispatch = useDispatch()
  const { user, isAuthenticated } = useSelector(state => state.auth)

  useEffect(() => {
    if (localStorage.token) {
      setAuthToken(localStorage.token)
      dispatch(setCurrentUser(localStorage.token))

      if (isAuthenticated && objectIsEmpty(user)) {
        dispatch(getCurrent())
      }
    }
  }, [isAuthenticated, user, dispatch])

  if (localStorage.token && objectIsEmpty(user)) {
    return <LoaderComponent />
  } else {
    return <WrappedComponent {...{ props }} />
  }
}

const AuthLoaderHOC = WrappedComponent => props => AuthLoader(WrappedComponent, props)

export default AuthLoaderHOC