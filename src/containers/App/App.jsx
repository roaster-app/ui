import React from 'react';
import Router from '../../routes'

import 'typeface-roboto'

const App = () => {

  return(
    <Router />
  )
}

export default App;
