import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { getCurrent } from '../../store/actions/users'

import { makeStyles } from '@material-ui/core/styles'
import {
  Typography,
  Paper,
  Grid
} from '@material-ui/core'

import LoaderComponent from '../../components/Loader/LoaderComponent'
import { objectIsEmpty } from '../../utils/custom-validator'

const Profile = () => {
  const dispatch = useDispatch()
  const { user } = useSelector(state => state.auth)

  useEffect(() => {
    dispatch(getCurrent())
  }, [dispatch])

  const classes = useStyles()

  const userInfo = <div>
    <Typography component="p">Username: {user.username}</Typography>
    <Typography component="p">Full Name: {user.fullName}</Typography>
    <Typography component="p">Email: {user.email}</Typography>
  </div>

  return (
    <Grid container component="main" spacing={2}>
      <Grid item xs={12} sm={6}>
        <Paper className={classes.paper}>
          <Typography variant="h5" component="h3">
            Account Information
          </Typography>
          <br/>
          {!objectIsEmpty(user) ? userInfo : <div className={classes.loader}><LoaderComponent small /></div>}
        </Paper>
      </Grid>
      <Grid item xs={12} sm={6}>
        <Paper className={classes.paper}>
          <Typography variant="h5" component="h3">
            Groups
          </Typography>
        </Paper>
      </Grid>
    </Grid>
  )
}

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white,
    },
  },
  paper: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing(2, 2),
  },
  loader: {
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center',
  }
}))

export default React.memo(Profile)