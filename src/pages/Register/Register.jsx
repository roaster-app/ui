import React from 'react'

import RegisterFormComponent from '../../components/RegisterForm/RegisterFormComponent'

const Register = props => {
  return(
    <RegisterFormComponent {...{ props }} />
  )
}

export default Register