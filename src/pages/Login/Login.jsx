import React from 'react'

import LoginFormComponent from '../../components/LoginForm/LoginFormComponent'

const Login = props => {
  return(
    <LoginFormComponent {...{ props }} />
  )
}

export default Login